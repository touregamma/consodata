package com.example.consodata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AppOpsManager;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Context context;
    private static final int READ_PHONE_STATE_REQUEST = 37;
    TextView networkStatsAllRx;
    TextView networkStatsAllTx;
    TextView stats;
    TextView imei;
    String IMEI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.context = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
             return ;
         }
        if (tm != null) {
            IMEI =  tm.getDeviceId();
            stats.setText("IMEI:"+ IMEI);
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        requestPermissions();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (!hasPermissions()) {
            return;
        }
        initTextViews();
        fillData();
    }
    private boolean hasPermissionToReadPhoneStats() {
        if (ActivityCompat.checkSelfPermission(this.context, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED) {
            return false;
        } else {
            return true;
        }
    }
    private void requestPhoneStateStats() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_REQUEST);
    }
    private void requestReadNetworkHistoryAccess() {
        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        startActivity(intent);
    }
    private boolean hasPermissionToReadNetworkHistory() {
        final AppOpsManager appOps = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                android.os.Process.myUid(), getPackageName());
        if (mode == AppOpsManager.MODE_ALLOWED) {
            return true;
        }
        appOps.startWatchingMode(AppOpsManager.OPSTR_GET_USAGE_STATS,
                getApplicationContext().getPackageName(),
                new AppOpsManager.OnOpChangedListener() {
                    @Override
                    public void onOpChanged(String op, String packageName) {
                        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                                android.os.Process.myUid(), getPackageName());
                        if (mode != AppOpsManager.MODE_ALLOWED) {
                            return;
                        }
                        appOps.stopWatchingMode(this);
                        fillData();
                    }
                });
        requestReadNetworkHistoryAccess();
        return false;
    }
    private void requestPermissions() {
        if (!hasPermissionToReadNetworkHistory()) {
            return;
        }
        if (!hasPermissionToReadPhoneStats()) {
            requestPhoneStateStats();
        }
    }
    private boolean hasPermissions() {
        return hasPermissionToReadNetworkHistory() && hasPermissionToReadPhoneStats();
    }
    private void initTextViews() {
        networkStatsAllRx = findViewById(R.id.network_stats_all_rx_value);
        networkStatsAllTx = findViewById(R.id.network_stats_all_tx_value);
        stats = findViewById(R.id.stats);
     }
    private void reinitTextViews() {
        networkStatsAllRx.setText("Total Received: ");
        networkStatsAllTx.setText("Total Send: ");
        stats.setText("Date: ");
        stats.setText("IMEI: ");
    }
    private void fillData() {
        NetworkStatsManager networkStatsManager = (NetworkStatsManager) getApplicationContext().getSystemService(Context.NETWORK_STATS_SERVICE);
        NetworkMonitor networkStatsHelper = new NetworkMonitor(networkStatsManager);
        fillNetworkStatsAll(networkStatsHelper);
    }
    private void fillNetworkStatsAll(NetworkMonitor networkStatsHelper) {
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        long total = -1;
        String startDate = "01/01/2019";
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                startDate = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (startDate != null) {
                    total = networkStatsHelper.getAllxMobile(this, startDate);
                }
            }
        }
        else {
            startDate = networkStatsHelper.getDate();
            total = networkStatsHelper.getAllxMobile(this);
        }
        reinitTextViews();
        long mobileRx = networkStatsHelper.getAllRxBytesMobile(this);
        networkStatsAllRx.setText(networkStatsAllRx.getText().toString() + mobileRx + " MB");
        long mobileTx = networkStatsHelper.getAllTxBytesMobile(this);
        networkStatsAllTx.setText(networkStatsAllTx.getText().toString() + mobileTx + " MB");
        String  IMEI = networkStatsHelper.getIMEI(this);
        stats.setText(stats.getText().toString() + startDate + " ---- Total data used: " + total + "MB");
        stats.setText(stats.getText().toString() + IMEI);

    }
}
