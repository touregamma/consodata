package com.example.consodata;

import android.Manifest;
import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import androidx.core.app.ActivityCompat;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NetworkMonitor {

    private NetworkStatsManager networkStatsManager;
    private Date fromDate;
    private Date toDate;
    private int constant = 1048576;

    public NetworkMonitor(NetworkStatsManager networkStatsManager) {
        this.networkStatsManager = networkStatsManager;
    }
    public long getAllxMobile(Context context) {
        NetworkStats.Bucket bucket;
        this.fromDate = getCurrentMonthStartDate();
        this.toDate = getCurrentMonthEndDate();
        try {
                bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    this.fromDate.getTime(),
                    toDate.getTime()
                );
            } catch (RemoteException e) {
                return -1;
            }
        return (bucket.getTxBytes() + bucket.getRxBytes()) / this.constant;
    }
    public long getAllxMobile(Context context, String startDate) {
        NetworkStats.Bucket bucket;
        SimpleDateFormat sdf = new SimpleDateFormat();
        try {
            Date dateFrom = sdf.parse(startDate);
            try {
                bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    dateFrom.getTime(),
                    System.currentTimeMillis()
                );
            } catch (RemoteException e) {
                return -1;
            }
        } catch (java.text.ParseException e) {
            return -1;
        }
        return (bucket.getTxBytes() + bucket.getRxBytes()) / this.constant;
    }
    public long getAllRxBytesMobile(Context context) {
        this.fromDate = getCurrentMonthStartDate();
        this.toDate = getCurrentMonthEndDate();

        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    this.fromDate.getTime(),
                    this.toDate.getTime()
            );
        } catch (RemoteException e) {
            return 0;
        }
        return bucket.getRxBytes() / this.constant;
    }
    public long getAllTxBytesMobile(Context context) {
        this.fromDate = getCurrentMonthStartDate();
        this.toDate = getCurrentMonthEndDate();
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    this.fromDate.getTime(),
                    this.toDate.getTime()
            );
        } catch (RemoteException e) {
            return 0;
        }
        return bucket.getTxBytes() / this.constant;
    }
    private String getSubscriberId(Context context, int networkType) {
        if (ConnectivityManager.TYPE_MOBILE == networkType) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "";
            }
            if (tm != null) {
                return tm.getSubscriberId();
            }
        }
        return "";
    }
    private Date getCurrentMonthStartDate(){
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }
    private Date getCurrentMonthEndDate(){
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }
    public String getDate(){
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        this.fromDate = getCurrentMonthStartDate();
        this.toDate = getCurrentMonthEndDate();
        String from_ = formatter.format(this.fromDate);
        String end_ = formatter.format(this.toDate);
        return from_ + " : " + end_;
    }
    public String getIMEI(Context context){
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return  tm.getDeviceId();
        }
        else
        {
            return  tm.getDeviceId();
        }

    }


}
